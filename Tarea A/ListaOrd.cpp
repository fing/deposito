/*
Lista de elementos de tipo tipoT ordenada de manera creciente, sin elementos
repetidos.

Laboratorio de Programación 3.
InCo-Fing-UDELAR
*/

//#ifndef _LISTAORD_H
//#define _LISTAORD_H

#include "tipoT.h"
#include "ListaOrd.h"
#include "stdlib.h"

struct AuxListaOrd{
    tipoT dat;
    AuxListaOrd sig;
};

typedef AuxListaOrd* ListaOrd;

void crearLista (ListaOrd &l)
/*  Devuelve en 'l' la lista vacía. */
{
  l = NULL;
};

void insLista (tipoT t, ListaOrd &l)
/*  Inserta 't', manteniendo el orden, en la lista 'l', si 't' no está en 'l'.
    En otro caso no hace nada. */
{
    if (l == NULL){
        l = new ListaOrd;
        l->dat = t;
        l->sig = NULL;
    }else {
        ListaOrd s = l;
        while ((s->sig || NULL) && !esMayor(s->dat,t)){
            s = s->sig;
        }
        if (!esIgual(s->dat,t)){
            s->sig = new ListaOrd;
            s = s->sig;
            s->sig = NULL;
            s->dat = t;
        }
    }
};

bool esVaciaLista (ListaOrd l)
/*  Devuelve 'true' si 'l' es vacía, 'false' en otro caso. */
{
  return (l == NULL);
};

tipoT primeroLista (ListaOrd l)
/*  Precondición: '! esVaciaLista (l)'.
    Devuelve el valor del primer elemento de 'l'. */
{
  return (l->dat);
};

void restoLista (ListaOrd & l)
/*  Precondición: '! esVaciaLista (l)'.
    Cambia 'l' a su resto.   */
{
  l = l->sig;
};

void destruirLista (ListaOrd &l)
/*  Libera toda la memoria ocupada por l. */
{
  ListaOrd t;
  while (l != NULL)
  {
    t = l->sig;
    delete l;
    l = t;
  }
};

/* _LISTA_H */
