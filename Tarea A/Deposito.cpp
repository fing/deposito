/*
Depósito de artículos representados por elementos de tipo tipoT.

Laboratorio de Programación 3.
InCo-Fing-UDELAR
*/
//atoi string a int
//itoa int to array se importan de stdlib
//
//#ifndef _DEPOSITO_H
//#define _DEPOSITO_H

#include "ListaOrd.h"
#include "Pila.h"
#include "tipoT.h"
#include "Deposito.h"
#include "stdlib.h"

   struct Celda {
	   	   ListaOrd referencias;
	   	   int	marcado;
      };

struct AuxDeposito {
	   Celda	*	Celdas	; // arreglo con celdas tipo Nodo
	   int		tamanio_del_arreglo	;
	   int		cantidad_articulos;

} ;

typedef AuxDeposito * Deposito;

Deposito crearDeposito (unsigned int cantidad_articulos)
/*  Devuelve un depósito con 'cantidad_articulos' artículos, sin referencias. */
{
      Deposito d = new AuxDeposito;
      d->Celdas = new Celda[cantidad_articulos];
      for (int i = 0; i < cantidad_articulos; i++){
    	crearLista(d->Celdas[i].referencias);
    	d-> Celdas[i].marcado = 0;
      }
      d->tamanio_del_arreglo = cantidad_articulos;
      d->cantidad_articulos = 0;

};

void agregarReferencia (Deposito d, tipoT a1, tipoT a2)
/*  Precondiciones:
        1. 0 <= 'hash (a1)' < cantidad de artículos de 'd'.
        2. 0 <= 'hash (a2)' < cantidad de artículos de 'd'.
    Establece en 'd' una referencia desde 'a1' hacia 'a2'. */
{
	int h = hash(a1);
	insLista (a2, d->Celdas[h].referencias);
	d->cantidad_articulos++;
};

ListaOrd referencias (Deposito d, tipoT a)
/*  Precondición: 0 <= 'hash (a)' < cantidad de artículos de 'd'.
    Devuelve la lista ordenada de las referencias de 'a'.
    La lista devuelta no debe compartir memoria con 'd'. */
{
  ListaOrd l;
  crearLista(l);
  ListaOrd r = d->Celdas[hash(a)].referencias;
  //ListaOrd primero = primeroLista (r);
  while (!esVaciaLista(r)){
      insLista(primeroLista(r),l);
      restoLista(r);
  }
  return l;
};

tipoT elemento (Deposito d, unsigned int pos)
/*  Precondición: 0 <= 'pos' < cantidad de artículos de 'd'.
    Devuelve el elemento x que cumple 'hash (x)' = 'pos'.   */
{
    return d->Celdas[pos].t;
}

unsigned int cantidadArticulos (Deposito d)
/*  Devuelve la cantidad de artículos de 'd'. */
{
    return d->tamanio_del_arreglo;
};


void dfs(Celda v, Deposito d, Pila &p)
{
	v.marcado = 1;
	ListaOrd r = v.referencias;
	if (!esVaciaLista(r)){
		ListaOrd s = restoLista(r);
		tipoT ref = hash(primeroLista(s));
		if (!esVaciaLista(restoLista(r) && d->Celdas[ref].marcado == 0){
			dfs(restoLista(r));
		}
		primeroLista(r);
	}
	apilar(primeroLista(r),p);
}

Pila dfsPostOrden (Deposito d)
/*  Devuelve una pila con todos los artículos de 'd' tras una recorrida dfs.
    Los apila en postorden. Cuando haya que decidir entre más de un artículo a
    visitar, se debe elegir el menor. */
{
	Pila p;
	crearPila(d->cantidad_articulos,p);
    for (int  i = 0	;i < d->tamanio_del_arreglo	;i++){
    	if (d->Celdas[i].marcado == 0){
    		dfs(d->Celdas[i],d,p);
    	}
    }
    return p;
    
};

Deposito transpuesto (Deposito d)
/*  Devuelve un depósito igual a 'd' pero con las referencias
    en sentido inverso. */
{
	Deposito d2 = crearDeposito(d->cantidad_articulos);
	for ( int j = 0; j< d->tamanio_del_arreglo ; j++){
		ListaOrd r = d->Celdas[j].referencias;

			while (!esVaciaLista(r)){
				tipoT t1 = hash(primeroLista(r));
				tipoT t2 = j;
				agregarReferencia(d2,t1,t2);
				restoLista(r);
			}
		}
	return d2;
}; 

void nuevosAccesibles (Deposito d, tipoT a, unsigned int id,
                       unsigned int * &agrupamientos)
/*  Precondiciones:
        1. El tamaño de 'agrupamientos' es igual a la cantidad de artículos de
           'd'.
        2. 'agrupamientos [hash (a)] == id'.

    El valor de 'agrupamientos [i]' es: el identificador del agrupamiento
    al que se ha asignado el artículo 'x' que cumple 'hash (x) == i', o
    USHRT_MAX si 'x' todavía no ha sido asignado a ningún agrupamiento.

    Modifica 'agrupamientos' asignando 'id' a los artículos que cumplen todas
    las siguientes condiciones:
        a) todavía no se les ha asignado agrupamiento;
        b) son accesibles desde 'a';
        c) el acceso desde 'a' debe poder hacerse sin seguir referencias a
  
 *          través de artículos a los que ya se había asignado agrupamiento antes
           de la invocación. */
{
	//prueba del git
};
                       
void destruirDeposito (Deposito &d)
/*  Libera la memoria asignada a d. */
{
    for (int i = 0; i<=d->tamanio_del_arreglo; i++){
        destruirLista(d->Celdas[i].referencias);
    }
    delete [] d->Celdas;
    delete d;
};

/* _DEPOSITO_H */
