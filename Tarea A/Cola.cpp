/* 4715801 */
#include "Cola.h"
#include "stdlib.h"
/*
Cola.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

struct AuxCola {
  tipoT dat;
  AuxCola* sig;
};
typedef AuxCola* Cola;

void crearCola (Cola & c)
/* Devuelve en c la cola vacia.*/
{
  c = NULL;
};

bool esVaciaCola (Cola c)
/* Devuelve 'true' si c es vacia, 'false' en otro caso.*/
{
  return (c == NULL);
};

void encolar (tipoT t, Cola &c)
/* Agrega el elemento t al final de c.*/
{
  if (c != NULL){
    Cola m = c;
    
    while (m->sig != NULL)
    {
     m = m->sig;
    }
     m->sig = new AuxCola;
      m->sig->dat = t;
      m->sig->sig = NULL;
    
  }
  else
  {
    c = new AuxCola;
    c->dat = t;
    c->sig = NULL;
  }
};

tipoT frente (Cola c)
/* Devuelve el primer elemento de c
   Precondicion: ! esVaciaCola(c).*/
{
    return c->dat;
};

void desencolar (Cola &c)
/* Remueve el primer elemento de c.
   Precondicion: ! esVaciaCola(c).*/
{
  Cola temp = c;
  c = c->sig;
  delete temp;
};

void destruirCola (Cola &c)
/* Libera toda la memoria ocupada por c.*/
{
  while (c != NULL)
  {
    desencolar(c);
  };
  delete c;
};

/* _COLA_H */