/*
Pila acotada de elementos de tipo tipoT.

Laboratorio de Programación 3.
InCo-Fing-UDELAR
*/

//#ifndef _PILA_H
//#define _PILA_H

#include "Pila.h"
#include "stdlib.h"

struct AuxPila
{
	lkkkkkml

    int tope;	// indica la posiciın del ıltimo elemento la pila
	int tamanio;	// indica la cantidad mıxima de elementos de la pila
	tipoT * elementos;
      };
      
typedef AuxPila* Pila;

void crearPila (int cota, Pila &p)
/*  Devuelve en 'p' la pila vacía, que podrá contener hasta 'cota' elementos. */
{
    p = new AuxPila;
    p->tope = -1;
    p->tamanio = cota;    
    p->elementos = new tipoT [cota];
    p.
};

bool esVaciaPila (Pila p)
/*  Devuelve 'true' si 'p' es vacía, 'false' en otro caso. */
{
    return (p->tope == -1);
};

bool esLlenaPila (Pila p)
/*  Devuelve 'true' si 'p' tiene cota elementos, donde cota es el valor del
    parámetro pasado en crearPila, 'false' en otro caso. */
{
    return (p->tope == p->tamanio);
};

void apilar (tipoT t, Pila &p)
/*  Si '!esLlenaPila (p)' inserta 't' en la cima de 'p', en otro caso no hace
    nada. */
{  
    if (!esLlenaPila(p)){
      p->tope = p->tope + 1;
      p->elementos[p->tope] = t;
    }
}

tipoT cima (Pila p)
/*  Precondición: '! esVaciaPila (p)'.
    Devuelve la cima de 'p'. */
{
    return (p->elementos[p->tope]);
}

void desapilar (Pila &p)
/*  Precondición: '! esVaciaPila (p)'.
    Remueve la cima de 'p'. */
{
    p->tope = p->tope - 1;
}

void destruirPila (Pila &p)
/*  Libera toda la memoria ocupada por 'p'. */
{
    delete [] p->elementos;
    delete p;
}

#endif /* _PILA_H */
